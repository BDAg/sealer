from selenium.webdriver import Chrome       #puxa o webdriver do Chrome da biblioteca Selenium
from time import sleep                      #puxa a função sleep
import csv                                  #importa a biblioteca CSV

     
Link=''
contlink=1
with open('confianca.csv', 'w', newline='') as csvfile:             #gera um arquivo csv
    fieldnames = ['Nome','Preço']
    spamwriter = csv.DictWriter(csvfile, delimiter=';', fieldnames=fieldnames)
    spamwriter.writeheader()

    while contlink <= 15 :
        #seleciona o link do site e abre o browser
        if contlink == 1:
            link = "https://www.confianca.com.br/padaria?"#1
        if contlink == 2:
            link = "https://www.confianca.com.br/matinais?"#2
        if contlink == 3:
            link = "https://www.confianca.com.br/mercearia#/"#3
        if contlink == 4:
            link = "https://www.confianca.com.br/emporium#/"#4
        if contlink == 5:
            link = "https://www.confianca.com.br/organicos#/"#5
        if contlink == 6:
            link = "https://www.confianca.com.br/hortifruti#/"#6
        if contlink == 7:
            link = "https://www.confianca.com.br/frios-e-laticinios#/"#7
        if contlink == 8:
            link = "https://www.confianca.com.br/congelados#/"#8
        if contlink == 9:
            link = "https://www.confianca.com.br/acougue#/"#9
        if contlink == 10:
            link = "https://www.confianca.com.br/bebidas#/"#10
        if contlink == 11:
            link = "https://www.confianca.com.br/alimentac-o-saudavel#/"#11
        if contlink == 12:
            link = "https://www.confianca.com.br/utilidades-domesticas#/"#12
        if contlink == 13:
            link = "https://www.confianca.com.br/limpeza#/"#13
        if contlink == 14:
            link = "https://www.confianca.com.br/higiene-e-beleza#/"#14 faltou essses 2
        if contlink == 15:
            link = "https://www.confianca.com.br/pet-shop#/"#15
        
        
        browser = Chrome()  #escolhe o browser a ser ultilizado                                               
        browser.get(link)
        sleep(5) #espera 5 segundos antes de puxar as informações para que de tempo de carregar

        #seleciona o botão "Mostrar mais"
        divB = browser.find_element_by_class_name('toolbar')
        botao = divB.find_element_by_tag_name('button')

        total = browser.find_element_by_class_name('amount')
        qnt = total.find_elements_by_tag_name('strong')
        print(total.text)

        r = int(qnt[1].text)
        divisao = int(r/24)
        cont=1
        while cont <= divisao :
            botao.click()
            sleep(5)
            cont += 1

        sleep(5)

        #pega os nomes e preços
        nome  = browser.find_elements_by_class_name('name')
        data = browser.find_elements_by_class_name('data')

        qnt = len(data)       #quantidade de produtos
        cont = 0              #contador

        print(qnt)

        while cont < qnt :         #while para mostrar na tela os nomes e adiciona-los ao arquivo csv     
            if data[cont].text == '' :
                data[cont] = 'Esgotado'
                spamwriter.writerow({'Nome': nome[cont].text,'Preço': data[cont]})
            else:
                spamwriter.writerow({'Nome': nome[cont].text,'Preço': data[cont].text})   

            print(f'{cont} {nome[cont].text}')
            cont += 1

        browser.quit()          #fecha o browser
        contlink += 1
    