<div align="center"><h1>Sealer</h1></div>
<pre>O projeto Sealer é um web crawler que tem o objetivo de mostrar o preço dos produtos fornecidos nos estabelecimentos 
Amigão e Confiança.​ Ele permite que o usuário acesse informações sobre os estabelecimentos de uma maneira mais simplificada 
e possibilita a criação de uma lista de compras personalizada para os moradores de Marília e região.</pre>
<hr/>

### Wiki
[Wiki do projeto](https://gitlab.com/BDAg/sealer/-/wikis/home)
<hr/>

### Equipe

[Caio Souza Lima](https://gitlab.com/BDAg/sealer/-/wikis/Caio-Souza-Lima)
<br>
[Helena Mattos Sotrate](https://gitlab.com/BDAg/sealer/-/wikis/Helena-Mattos-Sotrate)
<br>
[Janaina Pinheiro Gonçalez](https://gitlab.com/BDAg/sealer/-/wikis/Janaina-Pinheiro-Gon%C3%A7alez)
<br>
[Robson Ribeiro dos Santos](https://gitlab.com/BDAg/sealer/-/wikis/Robson-Ribeiro-dos-Santos)
<hr/>

### Tutoriais

**Instalando Python**
Tutorial do download e instalação do python:<br>
`Windows:` https://python.org.br/instalacao-windows/<br>
`Linux:` https://python.org.br/instalacao-linux/<br>
<br>

**Instalando bibliotecas**<br>
`Selenium:` https://pypi.org/project/selenium/