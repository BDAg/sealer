from selenium.webdriver import Chrome       #puxa o webdriver do Chrome da biblioteca Selenium
from time import sleep                      #puxa a função sleep
import csv                                  #importa a biblioteca CSV

link = ''    
contlink = 1

with open('Amigaoteste.csv', 'w', newline='') as csvfile:             #gera um arquivo csv
    fieldnames = ['Nome','Preço']
    spamwriter = csv.DictWriter(csvfile, delimiter=';', fieldnames=fieldnames)
    spamwriter.writeheader()

    while contlink <= 20 :  #seleciona o link do site
        if contlink == 1:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/limpeza"#1
        elif contlink == 2:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/alimentos-basicos"#2
        elif  contlink == 3:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/feira"#3
        elif  contlink == 4:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/carnes-aves-e-peixes"#4
        elif contlink == 5:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/bebidas"#5
        elif contlink == 6:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/bebidas-alcoolicas"#6
        elif contlink == 7:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/higiene-e-cuidados-pessoais"#7
        elif contlink == 8:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/biscoitos-e-salgadinhos"#8
        elif contlink == 9:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/frios-e-laticinios"#9
        elif contlink == 10:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/molhos-condimentos-e-conservas"#10
        elif contlink == 11:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/leites-e-iogurtes"#11
        elif contlink == 12:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/doces-e-sobremesas"#12
        elif contlink == 13:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/padaria"#13
        elif contlink == 14:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/matinais"#14
        elif contlink == 15:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/farinhas-e-graos"#15
        elif contlink == 16:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/congelados"#16
        elif contlink == 17:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/pet-shop-e-jardinagem"#17
        elif contlink == 18:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/utensilios-para-o-lar"#18
        else:
            link = "https://www.sitemercado.com.br/amigaosupermercados/marilia-loja-marilia-50-parque-sao-jorge-avenida-joao-ramalho/produtos/etnicos"#19

        
        browser = Chrome()      #seleciona o browser
        print(link)             #mostra o link selecionado
        browser.get(link)       #abre o site
        sleep(20)               #espera para carregar

        botao = browser.find_element_by_class_name('btn-light')  #seleciona o botao de filtro
        botao.click()       #clica no botao de filtro
        sleep(5)

        ul = browser.find_element_by_class_name('components')       #seleciona onde estão as listas
        li = ul.find_elements_by_tag_name('li')                     #pega as informações das listas

        listas = len(li)            #recebe a quantidade de listas
        contL=0
            
        while contL < listas:                       
            print(f'{li[contL].text}')
            sleep(10)
            li[contL].click()

            sleep(7) 
            browser.execute_script( "window.scrollTo (0, document.body.scrollHeight)" )         #desce até o final da pagina para carregar todas as informações
            nome = browser.find_elements_by_class_name('txt-desc-product-item')                 #busca o nome do produto
            preco = browser.find_elements_by_class_name('bloco-preco')                          #busca o preço do produto
            
            t = len(nome)       #recebe a quantidade de nomes

            cont = 0
            print(t)            #verifica a quantidade de nomes

            while cont < t:                      #envia o nome e preço do produto para o arquivo csv
                sleep(2)
                if (preco[cont].text == '&nbsp') or (preco[cont].text=='') :
                    preco[cont] = 'Esgotado'
                    spamwriter.writerow({'Nome': nome[cont].text,'Preço': preco[cont]})
                else:
                    spamwriter.writerow({'Nome': nome[cont].text,'Preço': preco[cont].text}) 

                print(f'{cont} {nome[cont].text}  {preco[cont].text}')
                cont += 1

            contL += 1
            
            browser.quit()              #fecha o browser
            sleep(7)
            browser = Chrome()          #seleciona o browser para abrir novamente
            browser.get(link)
            sleep(15)
            
            botao = browser.find_element_by_class_name('btn-light')             #seleciona o botão
            botao.click()               #clica no botão
            
            ul = browser.find_element_by_class_name('components')   #seleciona onde estão as listas
            li = ul.find_elements_by_tag_name('li')                 #pega as informações das listas
        contlink += 1